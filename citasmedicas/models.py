# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class Doctor(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='doctor')
    nombres = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50, blank=True)
    titulo = models.CharField(max_length=100)
    especialidad = models.CharField(max_length=50, blank=True, null=True)
    cedula_profesional = models.CharField(max_length=8)
    cedula_especialidad = models.CharField(max_length=8)
    registro_ssa = models.CharField(max_length=8)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    class Meta(object):
        verbose_name_plural = 'Doctores'

    def __str__(self):
        return self.nombres


class Secretaria(models.Model):
    doctor = models.ForeignKey(Doctor, related_name='secretaria')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombres = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50, blank=True)
    telefono_personal = models.IntegerField(blank=True, null=True)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    def __str__(self):
        return self.nombres


class Paciente(models.Model):
    doctor = models.ForeignKey(Doctor, related_name='pacientes')
    paciente = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=150)
    email = models.EmailField(null=True, blank=True)
    apellido_paterno = models.CharField(max_length=150)
    apellido_materno = models.CharField(max_length=150)
    telefono_personal = models.IntegerField()
    fecha_nacimiento = models.DateField()
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    def __str__(self):
        return self.nombre


class CatalogoEstados(models.Model):
    estado = models.CharField('Estado', max_length=100)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    class Meta(object):
        verbose_name_plural = 'Catalogo Estados'

    def __unicode__(self):
        return self.estado


class CatalogoMunicipio(models.Model):
    municipio = models.CharField('Municipio', max_length=100)
    estado = models.ForeignKey(CatalogoEstados)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    class Meta(object):
        verbose_name_plural = 'Catalogo Municipio'

    def __unicode__(self):
        return self.municipio


class Consultorio(models.Model):
    doctor = models.ForeignKey(Doctor)
    nombre = models.CharField('Nombre del consultorio', max_length=50)
    estado = models.ForeignKey(CatalogoEstados)
    municipio = models.ForeignKey(CatalogoMunicipio)
    colonia = models.CharField('Colonia', max_length=100)
    calle = models.CharField('Calle', max_length=100)
    cruce_a = models.CharField('Cruce A', max_length=100)
    cruce_b = models.CharField('Cruce B', max_length=100)
    numero_exterior = models.CharField('Numero exterior', max_length=100)
    numero_interior = models.CharField('Numero interior', max_length=100)
    telefono = models.CharField('Telefono', max_length=11)
    ext = models.CharField('Extencion', max_length=11)
    latitude = models.CharField(max_length=50)
    longitude = models.CharField(max_length=50)
    descripcion = models.TextField("Extras")
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)

    def __str__(self):
        return self.nombre


class Horario(models.Model):
    consultorio = models.ForeignKey(Consultorio)
    DIA_DOMINGO = 'DO'
    DIA_LUNES = 'LU'
    DIA_MARTES = 'MA'
    DIA_MIERCOLES = 'MI'
    DIA_JUEVES = 'JU'
    DIA_VIERNES = 'VI'
    DIA_SABADO = 'SA'
    DIA_SEMANA_CHOICES = (
        (DIA_DOMINGO, 'Domingo'),
        (DIA_LUNES, 'Lunes'),
        (DIA_MARTES, 'Martes'),
        (DIA_MIERCOLES, 'Miercoles'),
        (DIA_JUEVES, 'Jueves'),
        (DIA_VIERNES, 'Viernes'),
        (DIA_SABADO, 'Sabado'),
    )
    dia_semana = models.CharField(
        max_length=2,
        choices=DIA_SEMANA_CHOICES,
        default=DIA_LUNES,
    )
    hora_inicio = models.TimeField()
    hara_fin = models.TimeField()

    def __str__(self):
        return self.dia_semana


class Cita(models.Model):
    consultorio = models.ForeignKey(Consultorio)
    paciente = models.ForeignKey(Paciente)
    fecha = models.DateField()
    hora = models.TimeField()
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)


class Receta(models.Model):
    consultorio = models.ForeignKey(Consultorio)
    paciente = models.ForeignKey(Paciente)
    fecha = models.DateField()
    hora = models.TimeField()
    diagnostico = models.TextField()
    tratamiento = models.TextField()
    talla = models.CharField(max_length=20)
    peso = models.CharField(max_length=20)
    edad = models.CharField(max_length=20)
    creado = models.DateTimeField(auto_now_add=True)
    actualizado = models.DateTimeField(auto_now=True)
    activo = models.BooleanField("Activo", default=True)
