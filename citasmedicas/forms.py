# coding: utf-8
from django import forms
from citasmedicas.models import Secretaria, Paciente, Consultorio, Doctor
from django.contrib.auth.models import User
from django.contrib.admin.widgets import AdminDateWidget
from datetime import datetime


class LoginForm(forms.Form):
    usuario = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Usuario'}))
    contrasena = forms.CharField(label='Contraseña',
                                 widget=forms.PasswordInput(
                                     attrs={'class': 'form-control',
                                            'placeholder': 'Contraseña'}))


class SecretariaForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'email'}))
    contrasena = forms.CharField(label='Contraseña',
                                 widget=forms.PasswordInput(
                                     attrs={'class': 'form-control',
                                            'placeholder': 'Contraseña'}))
    repite_contrasena = forms.CharField(label='Repite contraseña',
                                        widget=forms.PasswordInput(
                                            attrs={'class': 'form-control',
                                                   'placeholder':
                                                   'Contraseña'}))

    class Meta:
        model = Secretaria
        fields = ('nombres', 'apellido_paterno',
                  'apellido_materno', 'telefono_personal')
        labels = {
            'telefono_personal': 'Teléfono personal'
        }
        widgets = {
            'nombres': forms.TextInput(attrs={'class': 'form-control',
                                              'placeholder': 'Nombres'}),
            'apellido_paterno': forms.TextInput(
                attrs={'class':
                       'form-control', 'placeholder': 'Apellido paterno'}),
            'apellido_materno': forms.TextInput(
                attrs={'class': 'form-control',
                       'placeholder': 'Apellido materno'}),
            'telefono_personal': forms.NumberInput(
                attrs={'class': 'form-control',
                       'placeholder': 'Teléfono personal',
                       'max': '9999999999'})
        }

    def clean_usuario(self):
        datos_limpios = self.cleaned_data
        usuario = datos_limpios.get('usuario')
        if User.objects.filter(username=usuario).exists():
            raise forms.ValidationError(
                "El usuario '%s' ya existe. Capture uno nuevo." % usuario)
        return usuario

    def clean_repite_contrasena(self):
        datos_limpios = self.cleaned_data
        contrasena = datos_limpios.get('contrasena')
        repite_contrasena = datos_limpios.get('repite_contrasena')
        if contrasena != repite_contrasena:
            raise forms.ValidationError(
                "La contraseña no es igual, intente de nuevo.")
        return repite_contrasena


class SecretariaEditForm(forms.ModelForm):

    class Meta:
        model = Secretaria
        fields = ('nombres', 'apellido_paterno',
                  'apellido_materno', 'telefono_personal')
        labels = {
            'telefono_personal': 'Teléfono personal'
        }
        widgets = {
            'nombres': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Nombres'}),
            'apellido_paterno': forms.TextInput(
                attrs={'class': 'form-control',
                       'placeholder': 'Apellido paterno'}),
            'apellido_materno': forms.TextInput(
                attrs={'class': 'form-control',
                       'placeholder': 'Apellido materno'}),
            'telefono_personal': forms.NumberInput(
                attrs={'class': 'form-control',
                       'placeholder': 'Teléfono personal',
                       'max': '9999999999'})
        }


class PacienteForm(forms.ModelForm):
    nombre = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Nombre'})
    )
    apellido_paterno = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Apellido paterno'})
    )
    apellido_materno = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Apellido materno'})
    )
    telefono_personal = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Teléfono personal',
               'max': '9999999999'})
    )
    email = forms.EmailField(
        widget=forms.EmailInput(attrs={'class': 'form-control'}),
        required=False
    )
    fecha_nacimiento = forms.DateField(widget=forms.SelectDateWidget(
        years=range(1980, datetime.now().year + 1)
    ))

    class Meta:
        model = Paciente
        fields = ('nombre', 'apellido_paterno',
                  'apellido_materno', 'telefono_personal',
                  'fecha_nacimiento')


class ConsultorioForm(forms.ModelForm):
    nombre = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Nombre del consultorio'}
    ))
    colonia = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Colonia'}))
    calle = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Calle'}))
    cruce_a = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Cruce'}))
    cruce_b = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Cruce'}),
                              required=False)
    numero_exterior = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Exterior'}))
    numero_interior = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'interior'}),
                                      required=False)
    telefono = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Telefono'}))
    ext = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder': 'Extencion'}),
                          required=False)
    latitude = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': 'readonly',
                                      })
    )
    longitude = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'readonly': 'readonly',
                                      })
    )

    descripcion = forms.CharField(
        widget=forms.Textarea(
            attrs={'class': 'form-control'}),
        required=False
    )

    class Meta:
        model = Consultorio
        fields = ('nombre', 'estado', 'municipio', 'colonia', 'calle',
                  'cruce_a', 'cruce_b', 'numero_exterior', 'numero_interior',
                  'telefono', 'ext', 'latitude', 'longitude', 'descripcion')

        widgets = {
            'estado': forms.Select(
                attrs={'class': 'form-control'}),
            'municipio': forms.Select(
                attrs={'class': 'form-control'}),
        }
