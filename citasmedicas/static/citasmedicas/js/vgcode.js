function goBack() {
    window.history.back();
}


$(document).ready(function () {
    var myLatLng = {
      lat: parseFloat($("#id_latitude").val()),
      lng: parseFloat($("#id_longitude").val())
    };
    //The options for the google map
    var myOptions = {
        zoom: 11,
        center: new google.maps.LatLng(20.959739, -89.5947259),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        position: myLatLng
    };
    var marker;

    function placeMarker(location) {
      if ( marker ) {
        marker.setPosition(location);
      } else {
        marker = new google.maps.Marker({
          position: location,
          map: map
        });
      }
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });

    google.maps.event.addListener(map, 'click', function( event ){
      $("#id_latitude").val(event.latLng.lat());
      $("#id_latitude").attr('value', event.latLng.lat());
      $("#id_longitude").val(event.latLng.lng());
      $("#id_longitude").attr('value', event.latLng.lng());
      placeMarker(event.latLng);
    });
});
