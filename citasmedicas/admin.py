# -*- coding: utf-8 -*-

from django.contrib import admin
from citasmedicas import models


class DoctorAdmin(admin.ModelAdmin):
    fields = ('usuario', 'nombres', 'apellido_paterno',
              'apellido_materno', 'titulo',
              'especialidad', 'cedula_profesional',
              'cedula_especialidad', 'registro_ssa')
    list_display = ('nombres', 'apellido_paterno',
                    'apellido_materno', 'titulo', 'especialidad')


class PacienteAdmin(admin.ModelAdmin):
    list_filter = ('nombre',)
    search_fields = ('nombre',)


class ConsultorioAdmin(admin.ModelAdmin):
    pass


@admin.register(models.CatalogoEstados)
class CatalogoEstadosAdmin(admin.ModelAdmin):
    pass

@admin.register(models.CatalogoMunicipio)
class CatalogoMunicipioAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Doctor, DoctorAdmin)
admin.site.register(models.Paciente, PacienteAdmin)
admin.site.register(models.Consultorio, ConsultorioAdmin)
